package javagame;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;


public class Pause extends BasicGameState{
	public String mouse = "NO";
	Image Background;
	boolean quit=false;
	
	public Pause(int state) {

	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{	
		Background=new Image("res/MenuBackground.png");
	}
	
	//Draw
	public void render(GameContainer gc, StateBasedGame sbg,Graphics g) throws SlickException {
		Background.draw(0,0);
			g.drawString("Resume (R)", 250,100);
			g.drawString("Main Menu (M)",250,150);
			g.drawString("Quit Game (Q)", 250, 200);
		
		}
	
	public void update(GameContainer gc, StateBasedGame sbg,int delta) throws SlickException{
		Input input = gc.getInput();
		if(input.isKeyDown(Input.KEY_R))
		sbg.enterState(1);
		if(input.isKeyDown(Input.KEY_M)) {
			
			sbg.enterState(0);
			}
		if(input.isKeyDown(Input.KEY_Q))
			System.exit(0);
	
	}
	
	public int getID() {
		return 6;
	}


}