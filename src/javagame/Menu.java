package javagame;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class Menu extends BasicGameState{
	public String mouse = "NO";
	Image classic,watermelon;
	Image arcade,banana;
	Image Background;
	Image gamename;
	Image quit,bomb;
	Image store,ninjamelon;
	
	public Menu(int state) {
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		
		Background = new Image("res/MenuBackground.png");
		classic = new Image("res/classic button.png");
		watermelon = new Image("res/Watermelon.png");
		arcade = new Image("res/arcade button.png");
		banana = new Image("res/Banana.png");
		quit = new Image("res/quit button.png");
		bomb = new Image("res/Bomb.png");
		gamename = new Image("res/gamename.png");
	}
	
	//Draw
	public void render(GameContainer gc, StateBasedGame sbg,Graphics g) throws SlickException {
		
		//set x and y axis
		Background.draw(0,0);
		gamename.draw(220, 10, 200, 112);
		classic.draw(70,150,210,130);
		classic.setCenterOfRotation(97, 67);
		classic.rotate(0.1f);
		watermelon.draw(140, 190, 0.6f);
		watermelon.setCenterOfRotation(28, 28);
		watermelon.rotate(-0.1f);
		arcade.draw(90,270,210,130);
		arcade.setCenterOfRotation(95, 70);
		arcade.rotate(0.1f);
		banana.draw(160, 310, 0.23f);
		banana.setCenterOfRotation(25, 30);
		banana.rotate(-0.1f);
		quit.draw(400, 290, 100, 100);
		quit.setCenterOfRotation(50, 50);
		quit.rotate(0.1f);
		bomb.draw(425, 310, 0.18f);
		bomb.setCenterOfRotation(25, 33);
		bomb.rotate(-0.1f);
		//g.drawString(mouse, 200, 400);
		}
	
	//animation movement
	public void update(GameContainer gc, StateBasedGame sbg,int delta) throws SlickException{
		
		int xpos = Mouse.getX();
		int ypos = Mouse.getY();
		//mouse = "Mouse position x: "+ xpos +" y: "+ ypos;
		Input input = gc.getInput();
		if(input.isKeyDown(Input.KEY_ENTER))
			sbg.enterState(1);
		//classic
		if((xpos>100 && xpos<230) && (ypos>155 && ypos<270))
			if(input.isMouseButtonDown(0))
				sbg.enterState(4);
		//arcade
		if((xpos>130 && xpos<240) && (ypos>20 && ypos<140))
			if(input.isMouseButtonDown(0))
				sbg.enterState(7);
		//quit
		if((xpos>395 && xpos<500) && (ypos>30 && ypos<130))
			if(input.isMouseButtonDown(0))
				System.exit(0);
	}
	
	public int getID() {
		return 0;
	}


}