package javagame;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.util.concurrent.ThreadLocalRandom;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.*;

public class PlayClassic extends BasicGameState{
	int ArcadeHighScore=0,ClassicHighScore=0 ;
	Sound Opening,Bombdisplay,Fruitdisplay,fruitcut,bombexplode,gameOver;
	public String mouse = "NO";
	int count =0;
	fruit[] fruits = new fruit[100];
	Image Apple,Banana,Bomb,Peach,Background,Pineapple,Lives,Livesx;
	boolean quit = false;
	float grav = 0.98f, deltaTime = 100;
	int[] duration = {200,200};
	int randomPos = ThreadLocalRandom.current().nextInt(1,7);
	float posdiffx = 80, posdiffy = 370;
	float shiftX =posdiffx*randomPos;
	float shiftY =posdiffy;
	double velX =6;
	double velY =15;
	Vector2f velocity, resistance;
	boolean onscreen=true;
	int lives =3;
	int randomNum = ThreadLocalRandom.current().nextInt(0,11);
	int score =0,i=0,flag=0,gameoverflag=0,startover=0;
	float shifttestx = posdiffx*randomPos;
	float shifttesty= posdiffy;
	int randomNumX = ThreadLocalRandom.current().nextInt(0,2);
	int randomRot = ThreadLocalRandom.current().nextInt(-1,1);
	float timer=0,frames=0;
	
	
	public PlayClassic(int state) {
	}
	

	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		Opening=new Sound("res/OpeningTheme.wav");
		Opening.play();
		Background = new Image("res/MenuBackground.png");
		Lives = new Image("res/Lives.png");
		Livesx= new Image ("res/LivesX.png");
		for(int x=0;x<100;x++) {
				fruits[x] = new fruit();
				randomNum=ThreadLocalRandom.current().nextInt(0,11);
				fruits[x].fruitImage= fruits[x].nextImagee(randomNum);
			}
		if(startover==1) {
			for(int x=0;x<100;x++) {
				fruits[x] = new fruit();
				randomNum=ThreadLocalRandom.current().nextInt(0,11);
				fruits[x].fruitImage= fruits[x].nextImagee(randomNum);
			}
		
		}
			}
	
	//Draw
	public void render(GameContainer gc, StateBasedGame sbg,Graphics g) throws SlickException {
		Background.draw(0,0);
		Lives.draw(500,1);
		Lives.draw(550,1);
		Lives.draw(590,1);
			if(lives<3) {
				Livesx.draw(497,-3);}
			if(lives<2) {
				Livesx.draw(547,-3);}
			if(lives<1) {
				Livesx.draw(587,-3);}
		if(lives<=0) {
			gameoverflag=1;
		}
		fruits[i].fruitImage.draw(fruits[i].shiftX,fruits[i].shiftY,50,50);
		fruits[i].fruitImage.setCenterOfRotation(25, 25);
		fruits[i].fruitImage.rotate(0.5f*randomRot);
	if(count>=3)	{
		fruits[i+1].fruitImage.draw(fruits[i+1].shiftX,fruits[i].shiftY,50,50);
		fruits[i+1].fruitImage.setCenterOfRotation(25, 25);
		fruits[i+1].fruitImage.rotate(0.5f*randomRot);
	}
			if(count>=5) {
			fruits[i+2].fruitImage.draw(fruits[i+2].shiftX,fruits[i].shiftY,50,50);
			fruits[i+2].fruitImage.setCenterOfRotation(25, 25);
			fruits[i+2].fruitImage.rotate(0.5f*randomRot);}
			
		g.drawString("Score: "+score, 100, 30);
		g.drawString("time: "+timer,100,70);

		try {
			g.drawString("HighScore: "+getClassicHighScore(), 100, 50);
		} catch (FileNotFoundException | JAXBException e1) {
			e1.printStackTrace();
		}
		if (gameoverflag==1) {
			setClassicHighScore(score,ArcadeHighScore);
			lives=4;
			gameoverflag=0;
			count=0;
			posdiffx = 80;
			posdiffy = 370;
			 shiftX =posdiffx*randomPos;
			 shiftY =posdiffy;
			 velX =6;
			 velY =15;
			 score =0;
			 i=0;
			 flag=0;
			 gameoverflag=0;
			 shifttestx = posdiffx*randomPos;
			 shifttesty= posdiffy;
			startover=1;
			GameOverMenu.setprevState(1);
			sbg.enterState(5);
		}
		if(quit==true) {
			lives =4;
			sbg.enterState(6);
			quit=false;
			count=0;
			posdiffx = 80;
			posdiffy = 370;
			 shiftX =posdiffx*randomPos;
			 shiftY =posdiffy;
			 velX =6;
			 velY =15;
			 score =0;
			 i=0;
			 flag=0;
			 gameoverflag=0;
			 shifttestx = posdiffx*randomPos;
			 shifttesty= posdiffy;
		}
	}
	
	
	//animation movement
	public void update(GameContainer gc, StateBasedGame sbg,int delta) throws SlickException{

		Input input = gc.getInput();
		int xpos = Mouse.getX();
		int ypos = Mouse.getY();
		motion();
		if(fruits[i].shiftX+50>=xpos&&fruits[i].shiftX<=xpos&&((((fruits[i].shiftY-370)+40>=ypos)||(fruits[i].shiftY-370)+40>=-ypos))&&(((fruits[i].shiftY-370)<=ypos)||(fruits[i].shiftY-370)<=-ypos)) {
		isSliced(gc,input,fruits[i]);
		}
		if(count>=3) {
		if(fruits[i+1].shiftX+50>=xpos&&fruits[i+1].shiftX<=xpos&&((((fruits[i+1].shiftY-370)+40>=ypos)||(fruits[i+1].shiftY-370)+40>=-ypos))&&(((fruits[i+1].shiftY-370)<=ypos)||(fruits[i+1].shiftY-370)<=-ypos)) {
			isSliced(gc,input,fruits[i+1]);	
		}}
		if(count>=5) {
			if(fruits[i+2].shiftX+50>=xpos&&fruits[i+2].shiftX<=xpos&&((((fruits[i+2].shiftY-370)+40>=ypos)||(fruits[i+2].shiftY-370)+40>=-ypos))&&(((fruits[i+2].shiftY-370)<=ypos)||(fruits[i+2].shiftY-370)<=-ypos)) {
				isSliced(gc,input,fruits[i+2]);	
		}
		}
		
		if(fruits[i].shiftY > 421){
			i++;
			if(fruits[i-1].sliced2!=true&&fruits[i-1].imageName!="res/Bomb.png") {
				lives--;
			}
			else
				fruits[i-1].sliced2=false;
			if(count>2) {
			if(fruits[i].sliced2!=true&&fruits[i].imageName!="res/Bomb.png") {
				lives--;
			}}
			else
				fruits[i].sliced2=false;
			if(count>4) {
				if(fruits[i+1].sliced2!=true&&fruits[i+1].imageName!="res/Bomb.png") {
					lives--;
				}
				else
					fruits[i+1].sliced2=false;
			}
			NewFruit(gc, sbg);
			Fruitdisplay=new Sound("res/Throw-fruit.wav");
			Fruitdisplay.play();
			shuffle(fruits[i]);
			count++;
					}
				

/*		if(input.isKeyDown(Input.KEY_UP))
		{
			fruits[i].shiftY -= delta *.1f;
		}
		if(input.isKeyDown(Input.KEY_DOWN))
		{
			fruits[i].shiftY += delta *.1f;
		}
		if(input.isKeyDown(Input.KEY_LEFT))
		{
			fruits[i].shiftX -= delta *.1f;
		}
		if(input.isKeyDown(Input.KEY_RIGHT))
		{			fruits[i].shiftX += delta *.1f;	
		}*/
			
		if(input.isKeyDown(Input.KEY_ESCAPE))
			quit=true;
		}
	
	public void NewFruit(GameContainer gc, StateBasedGame sbg) throws SlickException {
		randomPos = ThreadLocalRandom.current().nextInt(1,7);
		randomRot = ThreadLocalRandom.current().nextInt(-1,1);
		shiftX = posdiffx*randomPos;
		shiftY = posdiffy;
		velX = 6;
		velY = 15;
}
	public void shuffle(fruit fruits) throws SlickException {
		randomNum=ThreadLocalRandom.current().nextInt(0,11);
		while(fruits.randomNum==randomNum) {
			randomNum=ThreadLocalRandom.current().nextInt(0,11);		
		}
		fruits.fruitImage=fruits.nextImagee(randomNum);		
	}
	public void motion() {
		if(startover==1) {
			//float time = deltaTime * 0.00025f;
			//fruits[i+1].shiftX += (velX*time);
			//fruits[i+1].shiftY -= (velY*time*2.3);
			//velY -= grav*time;
			lives=4;
			startover=0;
		}
		else {
		float time = deltaTime * 0.001f;
		fruits[i].shiftX += (velX*time);
		//fruits[i].shiftX -= (velX*time);	
		fruits[i].shiftY -= (velY*time*2.3);
		velY -= grav*time;
		frames += time;
		if(frames >=60) {
			timer++;
			frames = 0;
		}

		if(fruits[i].shiftX > 590) {
			velX = -velX*0.8f;
		}
		if(fruits[i].shiftX < 1) {
			velX = -velX*0.8f;
		}
		if(fruits[i].shiftY < 1) {
			velY = -velY*1.2f;
		}
		}
	}
	
	public void isSliced(GameContainer gc,Input input,fruit fruits) throws SlickException{
		if(input.isMousePressed(0)) {
			fruits.sliced=true;
			fruits.sliced2 = true;
			if(fruits.imageName=="res/Bomb.png") {
				gameoverflag=1;
				bombexplode=new Sound("res/powerup-deflect-explode.wav");
				bombexplode.play();
			}else if(fruits.imageName=="res/Peach.png") {
				score=score+3;
			}else if(fruits.imageName=="res/specialfruit.png") {
				score=score+5;
			}else if(fruits.imageName=="res/dangerousbomb.png") {
				lives--;
			}else if(fruits.imageName=="res/specialfruit2.png") {
				score=score+10;
			}else if(fruits.imageName=="res/timebomb.png") {
				score=score-10;
			}else
				score++;
			fruits.fruitImage=fruits.nextImageeSliced(fruits.imageName);
			fruitcut=new Sound("res/Impact-Apple.wav");
			fruitcut.play();
		}
		fruits.sliced = false;
		}
	
	public int getClassicHighScore() throws JAXBException, PropertyException, FileNotFoundException  {
	   File file = new File("save.xml");
	   try {
		JAXBContext jaxbcontext = JAXBContext.newInstance(BestScore.class);
		Unmarshaller jaxbUnmarshaller = jaxbcontext.createUnmarshaller();
		BestScore Highscore = (BestScore) jaxbUnmarshaller.unmarshal(file);
		ArcadeHighScore = Highscore.getArcadescore();
		return Highscore.getClassicscore();
	   }
	   catch (JAXBException e) {
		   e.printStackTrace();
	   }
	return 0;
	}
	
	public void setClassicHighScore(int classicscore,int arcadescore) {
		   File file = new File("save.xml");
			BestScore Highscore = new BestScore(classicscore,arcadescore);
			try {
			JAXBContext jaxbcontext = JAXBContext.newInstance(BestScore.class);
			Marshaller jaxbmarshaller = jaxbcontext.createMarshaller();
			Unmarshaller jaxbUnmarshaller = jaxbcontext.createUnmarshaller();
			BestScore HighScore = (BestScore) jaxbUnmarshaller.unmarshal(file);
			if(HighScore.getClassicscore()>classicscore) {
			}
			else {
				jaxbmarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
				jaxbmarshaller.marshal(Highscore,file);
			}
		   }
		   catch (JAXBException e) {
			   e.printStackTrace();
		   }	
	}
	
	public int getID() {
		return 1;
	}
}